package ru.t1.bugakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.bugakov.tm.api.service.IDomainService;
import ru.t1.bugakov.tm.api.service.IServiceLocator;
import ru.t1.bugakov.tm.dto.request.data.*;
import ru.t1.bugakov.tm.dto.response.data.*;
import ru.t1.bugakov.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @Override
    @NotNull
    public DataBackupLoadResponse dataBackupLoad(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @Override
    @NotNull
    public DataBackupSaveResponse dataBackupSave(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @Override
    @NotNull
    public DataBase64LoadResponse dataBase64Load(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @Override
    @NotNull
    public DataBase64SaveResponse dataBase64Save(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @Override
    @NotNull
    public DataBinaryLoadResponse dataBinaryLoad(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @Override
    @NotNull
    public DataBinarySaveResponse dataBinarySave(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadFasterXmlResponse dataJsonLoadFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataJsonLoadJaxBResponse dataJsonLoadJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveFasterXmlResponse dataJsonSaveFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataJsonSaveJaxBResponse dataJsonSaveJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadFasterXmlResponse dataXmlLoadFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlLoadJaxBResponse dataXmlLoadJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveFasterXmlResponse dataXmlSaveFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataXmlSaveJaxBResponse dataXmlSaveJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @Override
    @NotNull
    public DataYamlLoadFasterXmlResponse dataYamlLoadFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataYamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @Override
    @NotNull
    public DataYamlSaveFasterXmlResponse dataYamlSaveFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getDomainService().dataYamlSaveFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
