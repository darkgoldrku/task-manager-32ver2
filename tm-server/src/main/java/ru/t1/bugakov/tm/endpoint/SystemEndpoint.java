package ru.t1.bugakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.IServiceLocator;
import ru.t1.bugakov.tm.dto.request.system.ServerAboutRequest;
import ru.t1.bugakov.tm.dto.request.system.ServerVersionRequest;
import ru.t1.bugakov.tm.dto.response.system.ServerAboutResponse;
import ru.t1.bugakov.tm.dto.response.system.ServerVersionResponse;

public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @Override
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @Override
    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
