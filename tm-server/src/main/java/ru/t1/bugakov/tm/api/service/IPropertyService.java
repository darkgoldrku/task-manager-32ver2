package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.salt.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

}
