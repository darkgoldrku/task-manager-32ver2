package ru.t1.bugakov.tm.repository;

import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

}
