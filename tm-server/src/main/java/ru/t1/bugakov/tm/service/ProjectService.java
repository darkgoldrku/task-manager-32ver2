package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.api.service.IProjectService;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.bugakov.tm.exception.field.*;
import ru.t1.bugakov.tm.model.Project;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
