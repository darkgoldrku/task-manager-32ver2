package ru.t1.bugakov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.task.*;
import ru.t1.bugakov.tm.dto.response.task.*;

public interface ITaskEndpoint extends IAbstractEndpoint {

    @NotNull TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull TaskClearResponse clearTasks(@NotNull TaskClearRequest request);

    @NotNull TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull TaskGetByIdResponse getTaskById(@NotNull TaskGetByIdRequest request);

    @NotNull TaskGetByIndexResponse getTaskByIndex(@NotNull TaskGetByIndexRequest request);

    @NotNull TaskListByProjectIdResponse listTasksByProjectId(@NotNull TaskListByProjectIdRequest request);

    @NotNull TaskListResponse listTasks(@NotNull TaskListRequest request);

    @NotNull TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request);

    @NotNull TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull TaskUnbindFromProjectResponse unbindTaskToProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request);

    @NotNull TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request);

}
