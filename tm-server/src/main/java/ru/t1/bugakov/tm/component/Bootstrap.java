package ru.t1.bugakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.*;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.api.service.*;
import ru.t1.bugakov.tm.dto.request.data.*;
import ru.t1.bugakov.tm.dto.request.project.*;
import ru.t1.bugakov.tm.dto.request.system.ServerAboutRequest;
import ru.t1.bugakov.tm.dto.request.system.ServerVersionRequest;
import ru.t1.bugakov.tm.dto.request.task.*;
import ru.t1.bugakov.tm.dto.request.user.*;
import ru.t1.bugakov.tm.endpoint.*;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.User;
import ru.t1.bugakov.tm.repository.ProjectRepository;
import ru.t1.bugakov.tm.repository.TaskRepository;
import ru.t1.bugakov.tm.repository.UserRepository;
import ru.t1.bugakov.tm.service.*;
import ru.t1.bugakov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);


    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProjects);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectGetByIdRequest.class, projectEndpoint::getProjectById);
        server.registry(ProjectGetByIndexRequest.class, projectEndpoint::getProjectByIndex);
        server.registry(ProjectListRequest.class, projectEndpoint::listProjects);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTasks);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskGetByIdRequest.class, taskEndpoint::getTaskById);
        server.registry(TaskGetByIndexRequest.class, taskEndpoint::getTaskByIndex);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::listTasksByProjectId);
        server.registry(TaskListRequest.class, taskEndpoint::listTasks);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskToProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changeUserPassword);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateUserProfile);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::dataBackupLoad);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::dataBackupSave);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::dataBase64Load);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::dataBase64Save);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::dataBinaryLoad);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::dataBinarySave);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::dataJsonLoadFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::dataJsonLoadJaxB);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::dataJsonSaveFasterXml);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::dataJsonSaveJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::dataXmlLoadFasterXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::dataXmlLoadJaxB);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::dataXmlSaveFasterXml);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::dataXmlSaveJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::dataYamlLoadFasterXml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::dataYamlSaveFasterXml);
    }

    private void initDemoData() {
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        projectService.create(admin.getId(), "p111", "222");
        taskService.create(admin.getId(), "t001", "222");
        taskService.create(admin.getId(), "t002", "444");

        @NotNull final User test = userService.create("test", "test", "test@tast.ru");
        projectService.create(test.getId(), "p333", "444");
        taskService.create(test.getId(), "t003", "222");
        taskService.create(test.getId(), "t004", "444");

        userService.create("user", "user", "user@user.ru");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start() {
        initPID();
        initDemoData();
        backup.start();
        server.start();
        loggerService.info("** WELCOME TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        loggerService.info("TASK MANAGER IS SHUTTING DOWN");
        backup.stop();
        server.stop();
    }

}