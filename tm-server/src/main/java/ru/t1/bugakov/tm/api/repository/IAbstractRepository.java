package ru.t1.bugakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull final M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull final Comparator<M> comparator);

    int getSize();

    @Nullable
    M findById(@NotNull final String id);

    @Nullable
    M findByIndex(@NotNull final Integer index);

    boolean existsById(@NotNull final String id);

    void clear();

    @Nullable
    M remove(@NotNull final M model);

    @Nullable
    M removeById(@NotNull final String id);

    @Nullable
    M removeByIndex(@NotNull final Integer index);

}
