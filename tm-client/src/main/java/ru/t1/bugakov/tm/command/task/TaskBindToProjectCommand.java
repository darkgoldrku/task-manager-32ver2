package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getTaskEndpoint().bindTaskToProject(new TaskBindToProjectRequest(projectId, taskId));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Bind task to project.";
    }

}
