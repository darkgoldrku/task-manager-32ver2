package ru.t1.bugakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.user.UserLogoutRequest;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthEndpoint().logout(new UserLogoutRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Logout user.";
    }

}
