package ru.t1.bugakov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.command.AbstractCommand;
import ru.t1.bugakov.tm.enumerated.Role;

import java.net.Socket;

public final class ServerConnectCommand extends AbstractCommand {

    @Override
    public void execute() {
        getServiceLocator().getAuthEndpoint().connect();
        final Socket socket = getServiceLocator().getAuthEndpoint().getSocket();
        getServiceLocator().getProjectEndpoint().setSocket(socket);
        getServiceLocator().getDomainEndpoint().setSocket(socket);
        getServiceLocator().getSystemEndpoint().setSocket(socket);
        getServiceLocator().getTaskEndpoint().setSocket(socket);
    }

    @Override
    public @NotNull
    String getName() {
        return "server-connect";
    }

    @Override
    public @Nullable
    String getArgument() {
        return null;
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Connect to server";
    }

    @Override
    public @NotNull
    Role[] getRoles() {
        return null;
    }
}
