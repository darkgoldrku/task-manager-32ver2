package ru.t1.bugakov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.bugakov.tm.dto.request.system.ServerAboutRequest;
import ru.t1.bugakov.tm.dto.request.system.ServerVersionRequest;
import ru.t1.bugakov.tm.dto.response.system.ServerAboutResponse;
import ru.t1.bugakov.tm.dto.response.system.ServerVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

}
