package ru.t1.bugakov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.bugakov.tm.dto.request.data.*;
import ru.t1.bugakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.bugakov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.bugakov.tm.dto.response.data.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpoint {

    public DomainEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    public DataBackupLoadResponse dataBackupLoad(@NotNull final DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    public DataBackupSaveResponse dataBackupSave(@NotNull final DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    public DataBase64LoadResponse dataBase64Load(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    public DataBase64SaveResponse dataBase64Save(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    public DataBinaryLoadResponse dataBinaryLoad(@NotNull final DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    public DataBinarySaveResponse dataBinarySave(@NotNull final DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    public DataJsonLoadFasterXmlResponse dataJsonLoadFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @NotNull
    public DataJsonLoadJaxBResponse dataJsonLoadJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @NotNull
    public DataJsonSaveFasterXmlResponse dataJsonSaveFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @NotNull
    public DataJsonSaveJaxBResponse dataJsonSaveJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @NotNull
    public DataXmlLoadFasterXmlResponse dataXmlLoadFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @NotNull
    public DataXmlLoadJaxBResponse dataXmlLoadJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @NotNull
    public DataXmlSaveFasterXmlResponse dataXmlSaveFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @NotNull
    public DataXmlSaveJaxBResponse dataXmlSaveJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        return call(request, DataXmlSaveJaxBResponse.class);
    }

    @NotNull
    public DataYamlLoadFasterXmlResponse dataYamlLoadFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @NotNull
    public DataYamlSaveFasterXmlResponse dataYamlSaveFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
            @NotNull final DomainEndpointClient domainEndpointClient = new DomainEndpointClient(authEndpointClient);
            domainEndpointClient.dataBase64Save(new DataBase64SaveRequest());
        }
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
            @NotNull final DomainEndpointClient domainEndpointClient = new DomainEndpointClient(authEndpointClient);
            domainEndpointClient.dataBase64Save(new DataBase64SaveRequest());
        }
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

}
