package ru.t1.bugakov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.bugakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.bugakov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.bugakov.tm.dto.request.user.UserProfileRequest;
import ru.t1.bugakov.tm.dto.response.result.UserLoginResponse;
import ru.t1.bugakov.tm.dto.response.result.UserLogoutResponse;
import ru.t1.bugakov.tm.dto.response.user.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @NotNull
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @NotNull
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    @NotNull
    public UserProfileResponse profile(@NotNull UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    public static void main(String[] args) {
        @NotNull final AuthEndpointClient client = new AuthEndpointClient();
        client.connect();
        System.out.println(client.profile(new UserProfileRequest()).getUser());

        System.out.println(client.login(new UserLoginRequest("user", "user")).getSuccess());
        System.out.println(client.profile(new UserProfileRequest()).getUser());

        System.out.println(client.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getLogin());

        client.disconnect();
    }

}
