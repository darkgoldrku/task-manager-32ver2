package ru.t1.bugakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    SystemEndpointClient getSystemEndpoint();

    AuthEndpointClient getAuthEndpoint();

    DomainEndpointClient getDomainEndpoint();

    ProjectEndpointClient getProjectEndpoint();

    TaskEndpointClient getTaskEndpoint();

    ru.t1.bugakov.tm.api.endpoint.IUserEndpoint getUserEndpoint();
}
