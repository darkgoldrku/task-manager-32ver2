package ru.t1.bugakov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.AbstractRequest;
import ru.t1.bugakov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(final RQ request);

}
