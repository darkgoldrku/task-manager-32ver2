package ru.t1.bugakov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.user.UserLoginRequest;
import ru.t1.bugakov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.bugakov.tm.dto.request.user.UserProfileRequest;
import ru.t1.bugakov.tm.dto.response.result.UserLoginResponse;
import ru.t1.bugakov.tm.dto.response.result.UserLogoutResponse;
import ru.t1.bugakov.tm.dto.response.user.UserProfileResponse;

public interface IAuthEndpoint extends IAbstractEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserProfileResponse profile(@NotNull UserProfileRequest request);

}
