package ru.t1.bugakov.tm.api.salt;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
