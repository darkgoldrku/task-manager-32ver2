package ru.t1.bugakov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.model.User;

@NoArgsConstructor
public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable final User user) {
        super(user);
    }

}
