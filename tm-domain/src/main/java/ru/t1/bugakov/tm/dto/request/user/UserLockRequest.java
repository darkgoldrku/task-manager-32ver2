package ru.t1.bugakov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserLockRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLockRequest(@Nullable String login) {
        this.login = login;
    }

}
