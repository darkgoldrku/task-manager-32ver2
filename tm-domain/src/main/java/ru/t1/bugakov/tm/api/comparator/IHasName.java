package ru.t1.bugakov.tm.api.comparator;

import org.jetbrains.annotations.NotNull;

public interface IHasName {

    @NotNull
    String getName();

    void setName(@NotNull final String name);

}
