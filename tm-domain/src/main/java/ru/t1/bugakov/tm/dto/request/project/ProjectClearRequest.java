package ru.t1.bugakov.tm.dto.request.project;

import lombok.NoArgsConstructor;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class ProjectClearRequest extends AbstractUserRequest {
}
