package ru.t1.bugakov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataJsonSaveFasterXmlRequest extends AbstractUserRequest {
}
